package main

import (
	"bytes"
	"crypto/tls"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
)

func main() {

	var (
		certFile = flag.String("certfile", "jwt.crt", "The cert file to potentially create.")
		certURL  = flag.String("url", "https://authentication.sgtec.io/certificate", "The url to use to authenticate.")
		insecure = flag.Bool("insecure", false, "Should we ignore certificate errors.")
		outfile  = flag.Bool("outfile", false, "Should we create the file directly?")
	)
	flag.Parse()

	// Setup the request
	request, err := http.NewRequest("GET", *certURL, nil)
	if err != nil {
		return
	}

	// Get a config to override potential SSL stuff.
	tlsConfig := &tls.Config{}

	// If we are insecure, then so be it.
	tlsConfig.InsecureSkipVerify = *insecure

	// Create the transport
	tr := &http.Transport{TLSClientConfig: tlsConfig}

	// Create a client using that transport.
	client := &http.Client{Transport: tr}

	// Make the request
	webResponse, err := client.Do(request)
	if err != nil {
		log.Fatalf("Error retrieving certificate: %v", err)
	}
	defer webResponse.Body.Close()

	switch webResponse.StatusCode {
	case http.StatusOK:
		break
	case http.StatusUnauthorized:
		log.Fatalf("Unable to retrieve certificate: unauthorized")
	case http.StatusInternalServerError:
		log.Fatalf("Unable to retrieve certificate: Internal Server Error")
	default:
		log.Fatalf("unexpected code received: %s", webResponse.Status)
	}

	// Get the body
	buffer := &bytes.Buffer{}
	io.Copy(buffer, webResponse.Body)
	if *outfile {
		file, err := os.Create("jwt.crt")
		if err != nil {
			log.Fatalf("Unable to open file: %s with error %v", *certFile, err)
		}
		io.Copy(file, buffer)
	} else {
		fmt.Printf("%s", buffer)
	}
}
