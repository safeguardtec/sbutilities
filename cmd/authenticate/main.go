package main

import (
	"bytes"
	"flag"
	"fmt"
	"io"
	"log"
	"os"

	"github.com/drhayt/jwtclient"
)

func main() {

	var (
		jwtcert  = flag.String("jwtcert", "jwt.crt", "The cert file to use")
		insecure = flag.Bool("insecure", false, "Ingore certificate validation issues.")
		url      = flag.String("url", "https://authentication.sgtec.io", "The url to use to authenticate.")
		username = flag.String("username", "BAD_USERNAME", "The username to authenticate against")
		password = flag.String("password", "BAD_PASSWORD", "The password to authenticate with")
	)

	flag.Parse()

	// get the validation cert.

	pemFile, err := os.Open(*jwtcert)
	if err != nil {
		errormsg := fmt.Errorf("Unable to open required file %s", *jwtcert)
		flag.Usage()
		panic(errormsg)
	}

	pemBytes := bytes.NewBuffer(nil)

	_, err = io.Copy(pemBytes, pemFile)
	if err != nil {
		panic(err)
	}

	config := jwtclient.Config{
		AuthKey:    *username,
		AuthSecret: *password,
		URL:        *url,
		Insecure:   *insecure,
	}

	client, err := jwtclient.New(&config)
	if err != nil {
		log.Fatalf("Unable to create client: %s", err)
	}

	token, err := client.RetrieveToken()
	if err != nil {
		log.Printf("Unable to retrieve token: %s", err)
	}
	log.Printf("Token is %s", token)

}
