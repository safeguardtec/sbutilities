package main

import (
	"flag"
	"fmt"
	"log"

	"github.com/drhayt/sbclient"
)

// PingRequest is the incoming message to the service bus.
type PingRequest struct {
	DelayMilliseconds int64
}

// PingResponse is the outgoing message from the service bus.
type PingResponse struct {
	PingResponse string
}

func main() {

	var (
		endpoint   = flag.String("endpoint", "http://servicebus/Execute.svc/Execute", "What servicebus endpoint to hit.")
		iterations = flag.Int("n", 1, "How many times to run the list.")
		duration   = flag.Int64("duration", 1, "How many miliseconds to sleep.")
		debug      = flag.Bool("debug", false, "Should we debug the service bus method?")
	)

	flag.Parse()

	utilityService := sbclient.New("Utility")

	// Override endpoint
	utilityService.SetUrl(*endpoint)

	// Enable debugging?
	if *debug {
		utilityService.Debug()
	}

	for i := 0; i < *iterations; i++ {
		Ping(utilityService, *duration)
	}
}

// Ping is a helper function to ping.
func Ping(sbclient *sbclient.Proxy, duration int64) {
	PReq := PingRequest{DelayMilliseconds: duration}
	PRep := PingResponse{}
	err := sbclient.Execute("Ping", PReq, &PRep)
	if err != nil {
		log.Printf("Ping failed: %v", err)
	}
	fmt.Printf("Ping: %v\n", PRep)
}
